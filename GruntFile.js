module.exports = function (grunt) {

  var destination = "dist";
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: [destination, '.tmp'],

        copy: {
            main: {
                expand: true,
                cwd: '',
                src: ['app/**/*.{js,html}', 'assets/*', 'index.html'],
                dest: destination +'/'
            }
        },

        rev: {
            files: {
                src: [destination +'/*.{js,css}', '!'+ destination +'/app/**']
            }
        },

        useminPrepare: {
            html: 'index.html'
        },

        usemin: {
            html: [destination +'/index.html']
        },

        uglify: {
            options: {
                report: 'min',
                mangle: false
            }
        },

        ftp_push: {
          prod: {
            options: {
              authKey: "prod",
              host: "milfoil.arvixe.com",
              dest: "/jeffdemers.com/wwwroot",
              port: 21
            },
            files: [{
                expand: true,
                cwd:    'dist',
                src:    ["**/*"],
              }]
          }
        },

        open:{
          prod:{
            path: "http://www.jeffdemers.com"
          }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-rev');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-package-modules');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-ftp-push');
    grunt.loadNpmTasks('grunt-open');

    // Tell Grunt what to do when we type "grunt" into the terminal
    grunt.registerTask('default', [
      'copy', 'useminPrepare', 'concat', 'uglify', 'cssmin', 'rev', 'usemin', 'ftp_push', 'open'
    ]);
};

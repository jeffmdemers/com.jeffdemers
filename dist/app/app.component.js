"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var person_service_1 = require('./services/person.service');
var profile_component_1 = require('./controllers/profile.component');
var education_component_1 = require('./controllers/education.component');
var workexperience_component_1 = require('./controllers/workexperience.component');
var skills_component_1 = require('./controllers/skills.component');
var AppComponent = (function () {
    function AppComponent(personService) {
        this.personService = personService;
    }
    AppComponent.prototype.getPerson = function () {
        var _this = this;
        this.personService.getPerson().then(function (person) { return _this.person = person; });
    };
    AppComponent.prototype.ngOnInit = function () {
        this.getPerson();
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'jeffdemers-app',
            directives: [profile_component_1.ProfileComponent, education_component_1.EducationComponent, workexperience_component_1.WorkExperienceComponent, skills_component_1.SkillsComponent],
            providers: [person_service_1.PersonService],
            templateUrl: '/app/views/person.html'
        }), 
        __metadata('design:paramtypes', [person_service_1.PersonService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map
"use strict";
var person_1 = require('../modals/person');
var location_1 = require('../modals/location');
var education_1 = require('../modals/education');
var workexperience_1 = require('../modals/workexperience');
var skill_1 = require('../modals/skill');
var webLink_1 = require('../modals/webLink');
exports.DemersData = new person_1.Person("Jeff Demers", "4/8/1985", new location_1.Location("22 Boyd Rd", "Londonderry", "NH", "03053"), "jeffmdemers@gmail.com", "603-723-3854", "jeffdemers.com", [
    new education_1.Education("Associates - English Literature", "Fall 2003", "Spring 2005", new education_1.School("New Hampshire Technical Institute", "Concord", "NH")),
    new education_1.Education("Associates - Computer Sciences", "Fall 2006", "Spring 2008", new education_1.School("New Hampshire Technical Institute", "Concord", "NH"))
], [
    new workexperience_1.WorkExperience("AutoTrader.com", "January 2010", "Present", "Application Developer & Team Lead", "I lead a team of developers who excel at presentation level coding, with most code written in C# ASP.NET using REST webservices and an MSSQL backend. I was brought in to this company in 2010 as the 2nd developer hired, and lead the charge in developing most of the applications that have made that small startup a success today. During my time here I have answered directly to our CTO, Matt Knox, and have been a key player in teaching and training our newer developers in both the business and code conventions we follow."),
    new workexperience_1.WorkExperience("HealthDrive", "June 2008", "January 2010", "Junior Software Developer", "As a junior developer I maintained and created applications for the internal team of administrators as well as for a team of doctors that services nursing homes. My duties includes fixing defects in legacy code, maintain tablet applications, and fielding calls from doctors as a tech support specialist. FoxPro and C# with MSSQL and webservices."),
    new workexperience_1.WorkExperience("wedu", "2007", "2008", "Intern (Developer)", "An internship that took me through the basic Web development process from within a small company. Jobs included working on previous made websites, creating new sites, altering code, web copywriting, and application and forms development. PHP with jQuery and CSS.")
], [
    new skill_1.Skill("C#", 90),
    new skill_1.Skill("ASP.NET", 80),
    new skill_1.Skill("MVC.NET", 80),
    new skill_1.Skill("SQL", 66),
    new skill_1.Skill("jQuery", 90),
    new skill_1.Skill("CSS", 100),
    new skill_1.Skill("HTML", 100),
    new skill_1.Skill("Bootstrap", 100),
    new skill_1.Skill("AngularJS", 66),
    new skill_1.Skill("Webservices", 80),
    new skill_1.Skill("PHP", 33),
    new skill_1.Skill("Java", 60),
    new skill_1.Skill("Android", 33),
    new skill_1.Skill("UI/UX", 50),
    new skill_1.Skill("SEO", 40)
], new webLink_1.webLink("jeffmdemers", "http://www.linkedin.com/in/jeffmdemers"));
//# sourceMappingURL=demers.data.js.map
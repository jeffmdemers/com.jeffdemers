"use strict";
var WorkExperience = (function () {
    function WorkExperience(employer, dateStart, dateEnd, position, description) {
        this.employer = employer;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.position = position;
        this.description = description;
    }
    return WorkExperience;
}());
exports.WorkExperience = WorkExperience;
//# sourceMappingURL=workexperience.js.map
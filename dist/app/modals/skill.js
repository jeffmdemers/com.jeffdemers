"use strict";
var Skill = (function () {
    function Skill(name, familiarity //1-100: 33: "rookie, 66: "good", 100: "Flawess"
        ) {
        this.name = name;
        this.familiarity = familiarity;
    }
    return Skill;
}());
exports.Skill = Skill;
//# sourceMappingURL=skill.js.map
"use strict";
var Education = (function () {
    function Education(degree, startDate, endDate, school) {
        this.degree = degree;
        this.startDate = startDate;
        this.endDate = endDate;
        this.school = school;
    }
    return Education;
}());
exports.Education = Education;
var School = (function () {
    function School(name, city, state) {
        this.name = name;
        this.city = city;
        this.state = state;
    }
    return School;
}());
exports.School = School;
//# sourceMappingURL=education.js.map
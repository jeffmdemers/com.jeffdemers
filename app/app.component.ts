import {Component} from '@angular/core';
import {Person} from './modals/person';
import {PersonService} from './services/person.service';
import {ProfileComponent} from './controllers/profile.component';
import {EducationComponent} from './controllers/education.component';
import {WorkExperienceComponent} from './controllers/workexperience.component';
import {SkillsComponent} from './controllers/skills.component';

@Component({
  selector: 'jeffdemers-app',
  directives: [ProfileComponent, EducationComponent, WorkExperienceComponent, SkillsComponent],
  providers: [PersonService],
  templateUrl: '/app/views/person.html'
})

export class AppComponent{
  person: Person;
 constructor(private personService: PersonService) { }
  getPerson() {
      this.personService.getPerson().then(person => this.person = person);
  }
  ngOnInit() {
      this.getPerson();
  }
}

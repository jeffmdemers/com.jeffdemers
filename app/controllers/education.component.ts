import { Component, Input } from '@angular/core';
import {Person} from '../modals/person';

@Component({
    selector: 'education',
    templateUrl: '/app/views/education.html'
})

export class EducationComponent{
    @Input('person') person: Person;
}

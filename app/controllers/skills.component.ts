import { Component, Input } from '@angular/core';
import {Person} from '../modals/person';

@Component({
    selector: 'skills',
    templateUrl: '/app/views/skills.html'
})

export class SkillsComponent{
    @Input('person') person: Person;

}

import { Component, Input } from '@angular/core';
import {Person} from '../modals/person';

@Component({
    selector: 'workexperience',
    templateUrl: '/app/views/workexperience.html'
})

export class WorkExperienceComponent{
    @Input('person') person: Person;
}

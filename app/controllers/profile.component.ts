import { Component, Input } from '@angular/core';
import {Person} from '../modals/person';

@Component({
    selector: 'profile',
    templateUrl: '/app/views/profile.html'
})

export class ProfileComponent{
    @Input('person') person: Person;
}

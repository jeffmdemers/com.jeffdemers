"use strict";
var Location = (function () {
    function Location(address, city, state, zip) {
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
    }
    return Location;
}());
exports.Location = Location;
//# sourceMappingURL=location.js.map
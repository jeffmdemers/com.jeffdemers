export class WorkExperience{
  constructor(
    public employer:string,
    public dateStart:string,
    public dateEnd:string,
    public position:string,
    public description:string
  ){  }
}

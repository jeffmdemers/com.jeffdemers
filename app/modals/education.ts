export class Education{
  constructor(
    public degree:string,
    public startDate:string,
    public endDate:string,
    public school:School
  ){  }
}

export class School{
  constructor(
    public name:string,
    public city:string,
    public state:string
  ){  }
}

"use strict";
var Person = (function () {
    function Person(name, dateOfBirth, address, email, phone, webAddress, education, workexperience, skills, linkedin) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.webAddress = webAddress;
        this.education = education;
        this.workexperience = workexperience;
        this.skills = skills;
        this.linkedin = linkedin;
    }
    return Person;
}());
exports.Person = Person;
//# sourceMappingURL=person.js.map
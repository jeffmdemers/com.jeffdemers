import {Location} from './location';
import {Education} from './education';
import {WorkExperience} from './workexperience';
import {Skill} from './skill';
import {webLink} from './webLink';

export class Person {
  constructor(
    public name:string,
    public dateOfBirth: string,
    public address: Location,
    public email:string,
    public phone:string,
    public webAddress:string,
    public education:Education[],
    public workexperience:WorkExperience[],
    public skills:Skill[],
    public linkedin: webLink
  ){  }
}

import {Injectable} from '@angular/core';
import {Person} from '../modals/person';
import {DemersData} from '../data/demers.data';

@Injectable()
export class PersonService{
  getPerson(){
    let person = DemersData;
    return Promise.resolve(person);
  }
}
